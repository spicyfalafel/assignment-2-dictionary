%define NEXT_ELEMENT 0

%macro colon 2
    %2:
    dq NEXT_ELEMENT
    db %1, 0
    %define NEXT_ELEMENT %2
%endmacro
