.PHONY: all clean

COMPILER=nasm
COMPILER_FLAGS=-felf64 -g

LINKER=ld
TARGET=dictionary

all: main.o dict.o lib.o
	$(LINKER) $? -o $(TARGET)

%.o: %.asm
	$(COMPILER) $(COMPILER_FLAGS) $< -o $@

clean:
	rm -f *.o
