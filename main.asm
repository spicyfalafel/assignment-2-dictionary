%include "lib.inc"

global _start
extern find_word

%define STDOUT 1
%define STDERR 2
%define BUFFER_SIZE 256

section .rodata
	too_long_error: db "Too long message", "\n", 0
	key_not_found_error: db "Key not found", "\n", 0

section .data
	%include "words.inc"

section .bss
	buffer: resb BUFFER_SIZE
section .text

_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word
	push rdx
	cmp rax, 0
	je .too_long

	mov rdi, buffer
	mov rsi, NEXT_ELEMENT
	call find_word
	cmp rax, 0
	je .key_not_found
	pop rdx
	lea rdi, [rax + 8 + 1 + rdx]
	mov rsi, STDOUT
	call print_string
	call exit

.too_long:
	mov rdi, too_long_error
	jmp .print_error_message

.key_not_found:
	mov rdi, key_not_found_error

.print_error_message:
	mov rsi, STDERR
	call print_string
	call exit
